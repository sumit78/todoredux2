import React from 'reat';
import {connect } from 'react-redux';

const TodoList = ({todos}) => (
  <ul>
    {todos.map((todo,index) => {
        return <li key = {index}>{todo}</li>
    })}
  </ul> 
)

export default connect(TodoList);