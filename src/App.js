import React from 'react';
import AddTodo from './containor/AddTodo'
import store from 'redux';
import {Provider} from 'react-redux';
function App() {
  return (
    <div>
      <Provider store={store}>
      <AddTodo/>
      </Provider>

    </div>
  );
}

export default App;
